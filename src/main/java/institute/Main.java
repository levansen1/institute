
package institute;

import java.util.ArrayList;

public class Main {
    public static void main(String[] args){
        Student s1 = new Student("jeff",1);
         Student s2 = new Student("Lev",2);
         
         ArrayList<Student> cse_students = new ArrayList<Student>();
         cse_students.add(s1);
         cse_students.add(s2);
         
         Department CSE = new Department("CSE", cse_students);
         ArrayList<Department> departments = new ArrayList<Department>();
         departments.add(CSE);
         
         Institute institute = new Institute("BITS", departments);
         
         System.out.println("Total students in Institute: " + institute.getTotalStudentsInInstitute());
           
        
    }
}
