/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package institute;

import java.util.ArrayList;

public class Institute {
 private String name;
 private ArrayList<Department> departments;

    public Institute(String name, ArrayList<Department> departments) {
        this.name = name;
        this.departments = departments;
    }
    
    public int getTotalStudentsInInstitute(){
        int noOfStudents = 0;
        ArrayList<Student> students;
        for(Department dept: departments ){
            students = dept.getStudents();
            for(Student s: students){
                noOfStudents++;
            }
        }
        return noOfStudents;
    }
    
}
